const Task = require("../models/task.js");

// All task
module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result;
	});
}

// Create task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

// Delete task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

// Update task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updateTask;
			}
		})
	})
}


//////////////////////////////////////
///////////////Activity///////////////
// Specific Task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((foundTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return foundTask;
		}
	})
}

// Update Status
module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = newContent.status;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}
