//Setup dependencies/modules
const express = require("express");
const mongoose = require("mongoose");

// routes
const taskRoute = require("./routes/taskRoute.js");

//Server Setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//DB Connection
mongoose.connect("mongodb+srv://admin:admin123@batch218-to-do.tb0qo8f.mongodb.net/toDo?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

//localhost:3001/tasks/ + add anoter endpoints
//localhost:3001/tasks/addNewTask

app.use("/task", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));
